console.disableYellowBox=true;
import React, {
	Component} from 'react';
import {
	TabNavigator} from 'react-navigation';
	
import TelaInicial from './src/TelaInicial';	

import PreCadastro from './src/PreCadastro';

import Config from './src/Config';

const NavigationApp = TabNavigator({
	Home:{
		screen:TelaInicial,	
	},
	PreCadastro:{
		screen:PreCadastro,
	},
	Config:{
		screen:Config,
	}		
},
	{
		tabBarOptions: {
			showIcon: true,
			activeTintColor: '#FFFFFF',
			backgroundColor: 'red',
			style : {
				backgroundColor: 'red',
	    }
		},
	}
);
export default NavigationApp; 