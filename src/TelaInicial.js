
import React, {
	Component} from 'react';
import {
	View, 
	Text,
	Button,
	TextInput,
	StyleSheet,
	Image}  from 'react-native';


export default class TelaInicial extends Component{

	static navigationOptions = ({navigation}) => ({
	
		tabBarLabel:'Home',
		tabBarIcon:({tintColor, focused}) => {
			if(focused){
				return (
					<Image source={require('../assets/images/home_on.png')} style={{backgroundColor: 'red', width:26, height:26}} />
				);
			}else
				return (
					<Image source={require('../assets/images/home_off.png')} style={{backgroundColor: 'red', width:26, height:26}} />
				);
		}
	});

	constructor(props){
		super(props);
		this.state = {nome:'Jonathan L.', cpf:''}
	
		this.enviarCPF = this.enviarCPF.bind(this);
	}

	enviarCPF(){

		this.props.navigation.navigate('PreCadastro', {
			nome:this.state.nome,
			cpf:this.state.cpf
		});

	}

	render(){
		return(
			<View style={styles.styleButton}>
				<Text>
					App - CRF-BA
				</Text>

				<TextInput  
					style={{height:40, borderWidth:1, borderColor:'#000000', width:200}} placeholder='CPF' onChangeText={(cpf)=>this.setState({cpf})}/>


				<Button title='Pré Cadastro' onPress={this.enviarCPF} />  
			
			</View>
		);
	}
}

const styles = StyleSheet.create({
	styleButton:{
		flex:1,
		alignItems:'center',
		paddingTop:100
	}
});